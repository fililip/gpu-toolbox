# GPU Toolbox

A simple Rust GL app to test vblank timings and GPU hangs on your system (and their consequences).

## Screenshot
![1](screenshots/1.png "screenshot 1")

## Clone
- Get latest Rust [here](https://www.rust-lang.org/learn/get-started)
- `git clone https://gitlab.com/fililip/gpu-toolbox`

## Run

```
cargo run --release
```

## Hang

Hitting the D key on your keyboard will enable the hanging shader, which should trigger some kind of GPU recovery mechanism on your system.

## Enjoy

:saluting_face:
