use std::{process::exit, sync::mpsc::Sender, time::Instant};

use tetra::{
    graphics::{self, mesh::GeometryBuilder, Color, DrawParams, Shader, Texture, TextureFormat},
    input::{self, Key},
    math::Vec2,
    time, window, Context, State, TetraError,
};

static MAX_DATA_POINTS: usize = 240;

pub struct Timeout {
    blank_texture: Texture,
    builder: GeometryBuilder,
    timeout_shader: Shader,
    timer: Instant,
    frame_counter: u128,
    frame_time_data: Vec<f32>,
    avg_frame_time: f32,
    frame_time_stdev: f32,
    draw_notifier: Sender<u8>,
    background_texture: Texture,
    background_texture_scroll: f32,
}

impl Timeout {
    pub fn new(ctx: &mut Context, draw_notifier: Sender<u8>) -> tetra::Result<Self> {
        let blank_texture =
            Texture::from_data(ctx, 1, 1, TextureFormat::Rgba8, &[255, 255, 255, 255])?;
        let timeout_shader =
            Shader::from_fragment_string(ctx, include_str!("../shaders/reset.frag"))?;

        timeout_shader.set_uniform(ctx, "u_crash", 0);

        let builder = GeometryBuilder::new();
        let background_texture =
            Texture::from_encoded(ctx, include_bytes!("../res/textures/background.jpg"))?;

        Ok(Self {
            blank_texture,
            builder,
            timeout_shader,
            timer: Instant::now(),
            frame_counter: 0,
            frame_time_data: vec![],
            avg_frame_time: 0.0,
            frame_time_stdev: 0.0,
            draw_notifier,
            background_texture,
            background_texture_scroll: 0.0,
        })
    }
}

fn calculate_stdev(data: &[f32]) -> f32 {
    let n = data.len();
    let mean = data.iter().sum::<f32>() / n as f32;
    let variance: f32 = data.iter().map(|&x| (x - mean).powf(2.0)).sum::<f32>() / (n as f32 - 1.0);

    variance.sqrt()
}

impl State for Timeout {
    fn update(&mut self, ctx: &mut Context) -> tetra::Result {
        let frame_time = time::get_delta_time(ctx).as_secs_f32() * 1000.0;
        self.frame_time_data.push(frame_time);

        if self.frame_time_data.len() > MAX_DATA_POINTS {
            self.frame_time_data
                .drain(0..self.frame_time_data.len() - MAX_DATA_POINTS);
        }

        let frame_time_sum = self.frame_time_data.iter().sum::<f32>();
        self.avg_frame_time = frame_time_sum / (self.frame_time_data.len() as f32);
        self.frame_time_stdev = calculate_stdev(&self.frame_time_data);

        if input::is_key_pressed(ctx, Key::F) {
            window::set_fullscreen(ctx, !window::is_fullscreen(ctx))?;
        }

        if input::is_key_pressed(ctx, Key::D) {
            self.timeout_shader.set_uniform(ctx, "u_crash", 1);
        }

        if input::is_key_pressed(ctx, Key::X) {
            self.timeout_shader.set_uniform(ctx, "u_crash", 0);
        }

        if input::is_key_pressed(ctx, Key::Q) {
            exit(0);
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> tetra::Result {
        let (window_width, window_height) = window::get_physical_size(ctx);
        let (window_width, window_height) = (window_width as f32, window_height as f32);

        let refresh_rate = window::get_refresh_rate(ctx)? as f32;
        let target_frame_time = 1000.0 / refresh_rate;

        let image_width = self.background_texture.width() as f32;
        let image_scale = window_height / self.background_texture.height() as f32;

        graphics::clear(ctx, Color::WHITE);
        let mut points = Vec::new();

        for (i, &frame_time) in self.frame_time_data.iter().enumerate() {
            points.push(Vec2::new(
                (MAX_DATA_POINTS - self.frame_time_data.len() + i) as f32 * 3.0,
                window_height / 2.0 - frame_time * 4.0,
            ));
        }

        self.builder.clear();
        let polyline = self.builder.polyline(1.2, &points)?;
        let mesh = polyline.build_mesh(ctx)?;

        let significant_deviation = {
            let mut significant_deviation = false;
            self.frame_time_data.iter().for_each(|frame_time| {
                if (frame_time - target_frame_time).abs() > 2.0 {
                    significant_deviation = true;
                }
            });
            significant_deviation
        };

        self.background_texture.draw(
            ctx,
            DrawParams::default()
                .position(Vec2::new(self.background_texture_scroll, 0.0))
                .scale(Vec2::one() * image_scale),
        );

        self.blank_texture.draw(
            ctx,
            DrawParams::default()
                .scale(Vec2::new(MAX_DATA_POINTS as f32 * 3.0, window_height))
                .color(Color::rgba(1.0, 1.0, 1.0, 0.5)),
        );

        let dims = [4, 4];
        for i in 0..dims[0] * dims[1] {
            self.blank_texture.draw(
                ctx,
                DrawParams::default()
                    .position(
                        Vec2::new((i / dims[0]) as f32, (i % dims[1]) as f32) * (80 + 16) as f32,
                    )
                    .scale(Vec2::one() * 80.0)
                    .color(if self.frame_counter % 2 == 0 {
                        Color::rgb(0.8, 0.1, 0.1)
                    } else {
                        Color::rgb(0.1, 0.8, 0.1)
                    }),
            );
        }

        mesh.draw(
            ctx,
            DrawParams::default().color(if significant_deviation {
                Color::RED
            } else {
                Color::BLACK
            }),
        );

        graphics::set_shader(ctx, &self.timeout_shader);
        self.blank_texture.draw(
            ctx,
            DrawParams::default().scale(Vec2::new(window_width, window_height)),
        );

        print!(
            "Draw command issued at: frame {}, timestamp {:.2}s, frametime {:.2}ms       \r",
            self.frame_counter,
            self.timer.elapsed().as_secs_f32(),
            time::get_delta_time(ctx).as_secs_f64() * 1000.0
        );

        self.draw_notifier
            .send(1)
            .map_err(|_| TetraError::PlatformError("oops".to_string()))?;

        graphics::reset_shader(ctx);

        if input::is_key_pressed(ctx, Key::D) {
            self.timeout_shader.set_uniform(ctx, "u_crash", 0);
            exit(0);
        }

        self.frame_counter += 1;
        self.background_texture_scroll += 480.0 / refresh_rate; // no delta time because judder

        if self.background_texture_scroll > window_width {
            self.background_texture_scroll = -image_width * image_scale;
        }

        Ok(())
    }
}
