#![windows_subsystem = "windows"]

use std::{
    env,
    process::exit,
    sync::mpsc::{self, TryRecvError},
    thread,
    time::{Duration, Instant},
};

use tetra::{time::Timestep, ContextBuilder};
use timeout::Timeout;

mod timeout;

fn main() -> tetra::Result {
    if cfg!(target_os = "linux") && env::var("SDL_VIDEODRIVER").is_err() {
        env::set_var("SDL_VIDEODRIVER", "wayland,x11");
    }

    let (tx, rx) = mpsc::channel::<u8>();

    thread::spawn(move || {
        let mut timer = Instant::now();
        let mut signaled = true;

        loop {
            match rx.try_recv() {
                Ok(_) => {
                    timer = Instant::now();
                    signaled = true;
                }
                Err(e) => {
                    if let TryRecvError::Disconnected = e {
                        break;
                    }
                }
            }

            let delta = timer.elapsed().as_secs_f32();

            if delta > 3.0 && signaled {
                signaled = false;
                eprintln!(
                    "\nGPU is taking too long to draw! Took more than {}ms",
                    delta * 1000.0
                );
            }

            if delta > 3.4 {
                eprintln!("\nGPU timeout or other hang detected; the app will now close.");
                exit(0);
            }

            thread::sleep(Duration::from_nanos(1));
        }
    });

    ContextBuilder::new("GPU Toolbox", 1280, 720)
        .fps_limit(false)
        .vsync(true)
        .resizable(true)
        .high_dpi(true)
        .timestep(Timestep::Variable)
        .show_mouse(true)
        .build()?
        .run(|ctx| Timeout::new(ctx, tx))
}
