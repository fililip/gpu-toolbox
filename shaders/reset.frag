#version 150

in vec2 v_uv;
in vec4 v_color;

uniform sampler2D u_texture;
uniform bool u_crash;

out vec4 o_color;

void main() {
    float additive = 0.5;

    if (u_crash) {
        float i = 0.5;

        while (i >= 0.0) {
            additive += 0.002 * cos(i * 2.0 * pow(2.0, 4.0));

            i++;
        }
    }

    o_color = v_color * vec4(additive);
}
